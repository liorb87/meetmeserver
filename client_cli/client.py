import requests
import json


class ClientCLI:
    def __init__(self, url, port, version):
        self.url = url
        self.port = port
        self.version = version
        self.base_url = 'http://' + self.url + ':' + str(self.port) + '/' + version + '/'

    def create_user(self, user):
        url = self.base_url + 'User/add/'
        params = {'entity': json.dumps(user)}
        headers = {'content-type': 'application/json',
                   'AUTHORIZATION': 'bla token_face'}

        response = requests.post(url, params=params, headers=headers)
        status_code = response.status_code
        json_data = json.loads(response.text)
        return status_code

    def login(self, event_id):
        pass

    def logout(self):
        pass

    def like(self, user_id):
        pass

    def unlike(self, user_id):
        pass

    def block(self, user_id):
        pass

    def send_msg(self, user_id):
        pass


def main():
    # url = '10.0.0.10'
    url = '192.168.1.139'
    port = '3000'
    version = 'v0.1'
    client_cli = ClientCLI(url, port, version)

    user = {'userName': 'lior2',
            'password': None,
            'facebook_id': '10155297589814351',
            'facebook_token': 'SDFhm456DFSDFghjhg',
            'firstName': 'Lior2',
            'lastName': 'Brafman2',
            'birthdate': '01/01/1990',
            'shortDescription': 'fake user',
            'picList': [],
            'sex': 'man',
            'sexualPreference': ['woman']}

    client_cli.create_user(user)
    pass

if __name__ == '__main__':
    main()

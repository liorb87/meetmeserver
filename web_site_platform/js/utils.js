$(document).ready(function(){
	// $('#ratingsGeneralField').slider({
		// formatter: function(value) {
			// return 'Current value: ' + value;
		// }
	// });
	function create_event_from_form() {
		var event = {};
		event["name"] = $("#nameField").val();
		event["bio"] = $("#bioField").val();
		event["address"] = $("#addresField").val();
		event["description"] = $("#descriptionField").val();
		event["ratings"] = {};
		event["ratings"]["opposite_sex_popularit"] = parseInt($("#ratingsOSPField").val());
		event["ratings"]["match_ratio"] = parseInt($("#ratingsMatchRatioField").val());
		event["ratings"]["sex_ration"] = parseInt($("#ratingsSexRationField").val());
		event["imageUrl"] = $("#imageField").val().split('\\').pop();

		event["location"] = {};
		event["location"]["lat"] = parseFloat($("#latField").val());
		event["location"]["lng"] = parseFloat($("#lngField").val());
		event["users_list"] = [];

		return event;
	}

    $("#submit_button1").click(function(e){
        e.preventDefault();
        var form = $('#EventUploadForm')[0];
        var data = new FormData(form);

        data.append("name", $("#nameField").val());
        data.append("location", {"lat": parseFloat($("#latField").val()), "lng": parseFloat($("#lngField").val())});

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "v0.1/HangoutLocation/add/",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                $("#result").text(data);
                console.log("SUCCESS : ", JSON.stringify(data, null, 2));
                alert("Event was added");

            },
            error: function (e) {
                $("#result").text(e.responseText);
                console.log("ERROR : ", JSON.stringify(e, null, 2));
                alert("Error, event was not added");

            }
        });
    });

	$("#submit_button").click(function(e){
		e.preventDefault();
		event = create_event_from_form();
		console.log(JSON.stringify(event, null, 4));

		$.ajax({
			url: "v0.1/HangoutLocation/add/",
			method: "POST",
			// headers: {
			// 'Authorization':'Basic xxxxxxxxxxxxx'
			// },
			// data: {"Authorization":"bGlvcjoxMjM0"},
			data: JSON.stringify(event),
			contentType: "application/json",
			dataType: 'json',
			success: function(data) {
				console.log("success: " + JSON.stringify(data, null, 2));
			},
			error: function(e) {
				console.log("error: " + JSON.stringify(e, null, 2));
			}
		});


	});

	$("#location_button").click(function(e){
	    e.preventDefault();
//	    coordinates_object = navigator.geolocation.getCurrentPosition(function(position) {
//	        console.log('lat: ' + position.coords.latitude);
//	        console.log('lng: ' + position.coords.longitude);
//	    });
        $.getJSON('https://ipinfo.io/geo', function(response) {
            var loc = response.loc.split(',');
            $("#latField").val(loc[0]);
            $("#lngField").val(loc[1]);
        });
	});

	$("#ratingsOSPField").on('input', function () {
	    $("#labelRatingsOSPField").text("opposite_sex_popularit: " + $("#ratingsOSPField").val());
    });
    $("#ratingsMatchRatioField").on('input', function () {
        $("#labelRatingsMatchRatioField").text("match_ratio: " + $("#ratingsMatchRatioField").val());
    });
    $("#ratingsSexRationField").on('input', function () {
        $("#labelRatingsSexRationField").text("sex_ration: " + $("#ratingsSexRationField").val());
    });
});
# pip install falcon
# pip install pyfcm
# pip install facebook-sdk


import time
import threading

from multiprocessing import Process, Pipe


from json import JSONDecodeError

from flask import Flask, request, json, send_file, redirect

import facebook
import urllib
from threading import Lock
from urllib.parse import urlparse
import requests
from random import *

from server import identity_pool_id, bucket_name, region_id, static_folder

app = Flask(__name__, static_folder=static_folder)

debug_mode = False
static_dns = 'ec2-18-217-0-234.us-east-2.compute.amazonaws.com'
version = 'v0.1'
port = 3000
db = None

likes = []
matches = []
server_matches = []
server_matches_lock = Lock()
likes_lock = Lock()

all_methods = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']

server_address = 'http://ec2-18-217-0-234.us-east-2.compute.amazonaws.com:80/v0.1/'
#server_address = 'http://192.168.1.135:3000/v0.1/'

def checkout_user( token):
    header = dict(
        AUTHORIZATION = 'facebookToken '+ token
    )
    requests.post(server_address + 'HangoutLocation/checkOut/', headers = header)



def checkin_user(location_id, token):
    header = dict(
        AUTHORIZATION = 'facebookToken '+ token
    )
    requests.post(server_address + 'HangoutLocation/checkIn/?id=' + location_id, headers = header)



def get_nearby_locations(lat, lng, token):
    header = dict(
        AUTHORIZATION = 'facebookToken '+ token
    )
    locations = requests.post(server_address + 'HangoutLocation/?radius=1&lat=' + lat + '&lng=' + lng + '&limit=10000&page=0', headers = header)
    locations_array = locations.json()['hangoutLocation']
    #print(len(locations_array))
    #print(locations.json())
    return locations_array

def get_suggestions( token):
    header = dict(
        AUTHORIZATION = 'facebookToken '+ token
    )
    users = requests.post(server_address + 'User/suggestions_in_event/', headers = header)
    users_array = json.loads(users.text)['User']
    #print(len(users_array))
   ## print(users_array.json())
    return users_array


def like( user_id, token):
    header = dict(
        AUTHORIZATION = 'facebookToken '+ token
    )
    resp = requests.post(server_address + 'User/like/?id=' + user_id, headers = header)
    #print(resp.text)
   ## print(users_array.json())
    return resp

def unlike( user_id, token):
    header = dict(
        AUTHORIZATION = 'facebookToken '+ token
    )
    resp = requests.post(server_address + 'User/unlike/?id=' + user_id, headers = header)
    #print(resp.text)
   ## print(users_array.json())
    return resp

def test_single_user(test_user, conn, matches_conn):
    my_likes =[]
    my_matches = []
    user_access_token = test_user['access_token']
    #print(str(user_access_token))
    header = dict(
        AUTHORIZATION='facebookToken ' + user_access_token
    )

    user_args = dict(deviceToken='147109385999430',
                     )
    user = requests.post(server_address +
        'User/login/?deviceToken=sdfsdfsdf',
        headers=header)
    print('user logged in')
    time.sleep(5)


    checkout_user(user_access_token)
    #print(user.json())
    try:
        if user is None or 'error' in user.json():
            print('user not yet registered')
            user_graph = facebook.GraphAPI(user_access_token)
            profile = user_graph.get_object('me',
                                            fields='first_name,last_name,birthday,interested_in,gender,picture.width(768).height(1024)')
            #print(profile)
            fb_firstName = profile['first_name']
            fb_lastName = profile['last_name']
            fb_id = profile['id']
            if profile['gender'] == 'male':
                fb_sex = 'man'
            else:
                fb_sex = 'woman'

            fb_user_name = 'facebook' + '_' + str(fb_id) + '_' + fb_firstName + '_' + fb_lastName
            new_user = dict(
                firstName=fb_firstName,
                facebook_id=fb_id,
                userName=fb_user_name,
                shortDescription="",
                birthdate="26/07/1999",
                lastName=fb_lastName,
                sexualPreference=[
                    'man', 'woman'
                ],
                picList=["1.jpg"],
                sex=fb_sex,
                deviceToken="c1V1DH1ACe0:APA91bG9v7OUVQTnDqjtuPgsf5tUh1JPdTlQr3gpaUl4MM-SsTtEQGuxgbN5o7VCQl-PtY_K3WxqxZKk8CcnPmic9IOsbkrn4D0-S13yaeSL3LyEGya0gZbtvLQAjMUHFOiSnETYeEIQ",

            )
            #print(new_user)
            entityString = json.dumps(new_user)

            resp = requests.post(server_address +
                'User/add/?entity=' + urllib.parse.quote(
                    entityString.encode('utf8')))
            #print(resp)
            time.sleep(2)
            user = requests.post( server_address +
                'User/login/?deviceToken=sdfsdfsdf',
                headers=header)
    except JSONDecodeError:
        print('error parsing user')

    #return
    print('user checked out')
    time.sleep(5)

    locations = get_nearby_locations('32.0927983', '34.7830983', user_access_token)
    location_index = randint(0, len(locations) - 1)
    #print(location_index)
    #print(locations[location_index]['_id'])
    print('user got hangout locations')
    time.sleep(5)
    checkin_user(locations[location_index]['_id'], user_access_token)
    print('user checked in')
    time.sleep(30)
    users = get_suggestions(user_access_token)
    print('user got suggestions')
    #time.sleep(50)
    #print(users)
    print('started swiping')
    for event_user in users:
        #time.sleep(1)
        succeeded = False;
        random_number = randint(0, 1)
        while succeeded is False:
            try:
                if random_number == 0:
                    like_resp = like(event_user['_id'], user_access_token)
                    resp_matches = json.loads(like_resp.text)['Match']
                    if not matches is None and not resp_matches[0] is None:
                        userA = resp_matches[0]['userA']
                        userB = resp_matches[0]['userB']
                        #server_matches_lock.acquire()
                        #server_matches.append([userA, userB])
                        #server_matches_lock.release()
                        my_matches.append([userA, userB])
                    #likes_lock.acquire()
                    my_likes.append([user.json()['User'][0]['_id'] ,event_user['_id']])
                    #likes_lock.release()

                else:
                    unlike(event_user['_id'], user_access_token)
                succeeded = True;
            except:
                print('connection error')
                time.sleep(10)
    conn.send(my_likes)
    matches_conn.send(my_matches)
    print('finished with ' + str(len(my_likes)) + ' likes')



class myThread (threading.Thread):
   def __init__(self, threadID, name, test_user):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.test_user = test_user
   def run(self):
      random_number = randint(1, 4)
      time.sleep(random_number)
      print ("Starting " + self.name)
      test_single_user(self.test_user)
      print ("Exiting " + self.name)

def test():
    oauth_args = dict(client_id='147109385999430',
                      client_secret='e8dcb7c28afb6b35a1efd7e8caf0fb7c',
                      grant_type='client_credentials')
    #oauth_curl_cmd = ['curl',
     #                 'https://graph.facebook.com/oauth/access_token?' + urllib.parse.urlencode(oauth_args)]
    #oauth_response = subprocess.Popen(oauth_curl_cmd,
    #                                  stdout=subprocess.PIPE,
    #                                  stderr=subprocess.PIPE).communicate()[0]
    file = requests.post('https://graph.facebook.com/oauth/access_token?', params = oauth_args)

#    print(str(file))

    #print(str(oauth_response))

 #   try:
  #      oauth_access_token = urllib.parse.parse_qs(str(oauth_response))['access_token'][0]
  #      print(oauth_access_token)
  #  except KeyError:
  #      print('Unable to grab an access token!')
  #      exit()

    token = file.json()['access_token']
    #print(token)
    facebook_graph = facebook.GraphAPI(token)
    test_users = facebook_graph.get_object('147109385999430/accounts/test-users', limit = 10)
    #print('total_users: ' + str(len(test_users['data'])))
    #test_single_user(test_users['data'][0])
    processes = []
    conns = []
    matches_conns = []
    index = 0
    for test_user in test_users['data']:
        parent_conn, child_conn = Pipe()
        matches_parent_conn, matches_child_conn = Pipe()
        my_process = Process(target = test_single_user, args=(test_users['data'][index],child_conn, matches_child_conn, ) )
        my_process.start()
        index = index + 1
        processes.append(my_process)
        conns.append(parent_conn)
        matches_conns.append(matches_parent_conn)

    for p in processes:
        p.join()
    for c in conns:
        sub_likes = c.recv()
        likes.extend(sub_likes)
    for c in matches_conns:
        sub_matches = c.recv()
        server_matches.extend(sub_matches)


    print('likes:\n' + str(likes))
    for single_like in likes:
        userA = single_like[0]
        userB = single_like[1]
        reverse_like = [userB, userA]
        if reverse_like in likes and not      single_like in matches:
            matches.append(reverse_like)
    print('matches:\n' + str(matches))

    print('server matches:\n' + str(server_matches))

    num_of_server_matches_matching_local = 0

    for server_match in server_matches:
        userA = server_match[0]
        userB = server_match[1]
        reverse_match = [userB, userA]
        if reverse_match in matches or server_match in matches:
            num_of_server_matches_matching_local = num_of_server_matches_matching_local + 1
        else:
            print('match not found locally: ' + str(server_match) + '\n')

    print('local matches count:\n' + str(len(matches)))
    print('server matches count:\n' + str(len(server_matches)))
    print('server matches found locally:\n' + str(num_of_server_matches_matching_local))


if __name__ == '__main__':
    test()




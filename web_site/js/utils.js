$(document).ready(function(){
    new WOW().init();
    // console.log("hello");
    $("#submit_btn").click(function(event) {
      event.preventDefault();
      var re = /\S+@\S+\.\S+/;
      var name = $("#name_input").val();
      var email = $("#email_input").val();
      if (name == "") {
        alertify.error("you didn't provide your name");
      }
      else if (email == "") {
        alertify.error("you didn't provide your email");
      }
      else if (!re.test(email)) {
        alertify.error("the email you provided is invalid");
      }
      else {
        $.ajax({
			url: "v0.1/Subscribe/",
			method: "GET",
			data: {"name": name,
			       "email": email},
//			contentType: "application/json",
			dataType: 'json',
			success: function(data) {
				alertify.success("thank you " + data.name + ", you successfully subscribed " + data.email + " to our News Letter");
			},
			error: function(e) {
				alertify.error("error: " + JSON.stringify(e, null, 2));
			}
		});
      }
    });
});
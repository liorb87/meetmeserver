from dal.documents_type.user import User, AgePreference
from dal.enums.sex import Sex

lior = User()
lior.userName = 'liorb87'
lior.password = '123456'
lior.facebook_id = '10155297589814352'
lior.facebook_token = '10155297589814352'
lior.firstName = 'Lior'
lior.lastName = 'Brafman'
lior.birthdate = '18/01/1987'
lior.shortDescription = '30, from herlzily, programer'
lior.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
lior.sex = Sex.man
lior.sexualPreference = [Sex.woman]
lior.AgePreference = AgePreference(16, 40)
lior.likes = []

jacob = User()
jacob.userName = 'jacob_zviagin'
jacob.password = '4321'
jacob.facebook_id = '10155999008898498'
jacob.facebook_token = '10155999008898498'
jacob.firstName = 'Jacob'
jacob.lastName = 'Zviagin'
jacob.birthdate = '30/05/1983'
jacob.shortDescription = 'from tel aviv, android app freelancer'
jacob.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
jacob.sex = Sex.man
jacob.sexualPreference = [Sex.woman]
jacob.AgePreference = AgePreference(18, 32)
jacob.likes = []

bar = User()
bar.userName = 'bar_refaeli'
bar.password = '2468'
bar.facebook_id = ''
bar.facebook_token = ''
bar.firstName = 'Bar'
bar.lastName = 'Refaeli'
bar.birthdate = '04/06/1985'
bar.shortDescription = 'living in tel aviv, model, television host, actress, and businesswoman'
bar.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
bar.sex = Sex.woman
bar.sexualPreference = [Sex.man]
bar.AgePreference = AgePreference(30, 50)
bar.likes = []

katy = User()
katy.userName = 'katy_perry'
katy.password = 'qwe45'
katy.facebook_id = ''
katy.facebook_token = ''
katy.firstName = 'Katy'
katy.lastName = 'Perry'
katy.birthdate = '25/08/1984'
katy.shortDescription = 'living in USA, singer and songwriter'
katy.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
katy.sex = Sex.woman
katy.sexualPreference = [Sex.man, Sex.woman]
katy.AgePreference = AgePreference(30, 50)
katy.likes = []

gal = User()
gal.userName = 'gal_raviv'
gal.password = 'qwe45'
gal.facebook_id = ''
gal.facebook_token = ''
gal.firstName = 'Gal'
gal.lastName = 'Raviv'
gal.birthdate = '25/08/1984'
gal.shortDescription = ''
gal.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
gal.sex = Sex.man
gal.sexualPreference = [Sex.man, Sex.woman]
gal.AgePreference = AgePreference(30, 50)
gal.likes = []


ori = User()
ori.userName = 'ori_berger'
ori.password = 'qwe45'
ori.facebook_id = ''
ori.facebook_token = ''
ori.firstName = 'Ori'
ori.lastName = 'Berger'
ori.birthdate = '25/08/1984'
ori.shortDescription = ''
ori.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
ori.sex = Sex.man
ori.sexualPreference = [Sex.man, Sex.woman]
ori.AgePreference = AgePreference(30, 50)
ori.likes = []


rachel = User()
rachel.userName = 'rachel_shalom'
rachel.password = 'qwe45'
rachel.facebook_id = ''
rachel.facebook_token = ''
rachel.firstName = 'Rachel'
rachel.lastName = 'Shalom'
rachel.birthdate = '25/08/1984'
rachel.shortDescription = ''
rachel.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
rachel.sex = Sex.man
rachel.sexualPreference = [Sex.man, Sex.woman]
rachel.AgePreference = AgePreference(30, 50)
rachel.likes = []



gilad = User()
gilad.userName = 'gilad_spiegel'
gilad.password = 'qwe45'
gilad.facebook_id = ''
gilad.facebook_token = ''
gilad.firstName = 'Gilad'
gilad.lastName = 'Spiegel'
gilad.birthdate = '25/08/1984'
gilad.shortDescription = ''
gilad.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
gilad.sex = Sex.man
gilad.sexualPreference = [Sex.man, Sex.woman]
gilad.AgePreference = AgePreference(30, 50)
gilad.likes = []


brad = User()
brad.userName = 'brad_pitt'
brad.password = 'qwe45'
brad.facebook_id = ''
brad.facebook_token = ''
brad.firstName = 'Brad'
brad.lastName = 'Pitt'
brad.birthdate = '25/08/1984'
brad.shortDescription = ''
brad.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
brad.sex = Sex.man
brad.sexualPreference = [Sex.man, Sex.woman]
brad.AgePreference = AgePreference(30, 50)
brad.likes = []


leonardo = User()
leonardo.userName = 'leonardo_dicaprio'
leonardo.password = 'qwe45'
leonardo.facebook_id = ''
leonardo.facebook_token = ''
leonardo.firstName = 'Leonardo'
leonardo.lastName = 'Dicaprio'
leonardo.birthdate = '25/08/1984'
leonardo.shortDescription = ''
leonardo.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
leonardo.sex = Sex.man
leonardo.sexualPreference = [Sex.man, Sex.woman]
leonardo.AgePreference = AgePreference(30, 50)
leonardo.likes = []

george = User()
george.userName = 'george_clooney'
george.password = 'qwe45'
george.facebook_id = ''
george.facebook_token = ''
george.firstName = 'George'
george.lastName = 'Clooney'
george.birthdate = '25/08/1984'
george.shortDescription = ''
george.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
george.sex = Sex.man
george.sexualPreference = [Sex.man, Sex.woman]
george.AgePreference = AgePreference(30, 50)
george.likes = []

mariah = User()
mariah.userName = 'mariah_carey'
mariah.password = 'qwe45'
mariah.facebook_id = ''
mariah.facebook_token = ''
mariah.firstName = 'Mariah'
mariah.lastName = 'Carey'
mariah.birthdate = '25/08/1984'
mariah.shortDescription = ''
mariah.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
mariah.sex = Sex.man
mariah.sexualPreference = [Sex.man, Sex.woman]
mariah.AgePreference = AgePreference(30, 50)
mariah.likes = []


rihanna = User()
rihanna.userName = 'rihanna'
rihanna.password = 'qwe45'
rihanna.facebook_id = ''
rihanna.facebook_token = ''
rihanna.firstName = 'Rihanna'
rihanna.lastName = ''
rihanna.birthdate = '25/08/1984'
rihanna.shortDescription = ''
rihanna.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
rihanna.sex = Sex.man
rihanna.sexualPreference = [Sex.man, Sex.woman]
rihanna.AgePreference = AgePreference(30, 50)
rihanna.likes = []


beyonce = User()
beyonce.userName = 'beyonce_knowles'
beyonce.password = 'qwe45'
beyonce.facebook_id = ''
beyonce.facebook_token = ''
beyonce.firstName = 'Beyonce'
beyonce.lastName = 'Knowles'
beyonce.birthdate = '25/08/1984'
beyonce.shortDescription = ''
beyonce.picList = ['0.jpg', '1.jpg', '2.jpg', '3.jpg']
beyonce.sex = Sex.man
beyonce.sexualPreference = [Sex.man, Sex.woman]
beyonce.AgePreference = AgePreference(30, 50)
beyonce.likes = []


all_users = [brad, leonardo, george, mariah, beyonce, rihanna]

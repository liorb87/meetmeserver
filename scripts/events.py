from dal.documents_type.event import Event, Location, Rating


dizzy = Event()
dizzy.name = 'Dizzy'
dizzy.address = 'Dizenghoff 34 Tel Aviv'
dizzy.description = 'very nice place'
dizzy.location = Location(32.074902, 34.777540)
dizzy.bio = 'nice pickup bar'
dizzy.imageUrl = 'dizzy_frishdon.jpg'
dizzy.ratings = Rating(sex_ration=6, opposite_sex_popularit=7, match_ratio=8)
dizzy.users_list = []

el_vacino = Event()
el_vacino.name = 'El vacino'
el_vacino.address = 'Ibn Gabirol 189 Tel Aviv'
el_vacino.description = 'wine bar'
el_vacino.location = Location(32.094541, 34.783357)
el_vacino.bio = 'and also good food'
el_vacino.imageUrl = 'elvecino.png'
el_vacino.ratings = Rating(sex_ration=2, opposite_sex_popularit=0, match_ratio=1)
el_vacino.users_list = []

inga = Event()
inga.name = 'inga'
inga.address = 'Galgalei ha-Plada 16, Herzliya'
inga.description = 'A bur with people of all ages'
inga.location = Location(32.165260, 34.809554)
inga.bio = 'nice little place'
inga.imageUrl = 'inga.jpg'
inga.ratings = Rating(sex_ration=6, opposite_sex_popularit=7, match_ratio=8)
inga.users_list = []

drink_point = Event()
drink_point.name = 'Drink Point'
drink_point.address = 'Ben yehuda 142 Tel Aviv'
drink_point.description = 'low cost bar'
drink_point.location = Location(32.086039, 34.772812)
drink_point.bio = 'Very popular bar'
drink_point.imageUrl = 'drink_point.png'
drink_point.ratings = Rating(sex_ration=10, opposite_sex_popularit=9, match_ratio=9)
drink_point.users_list = []

cool_bar = Event()
cool_bar.name = 'Cool Bar'
cool_bar.address = 'hashmonaim 91 tel aviv'
cool_bar.description = 'the hottest place'
cool_bar.location = Location(32.069991, 34.783227)
cool_bar.bio = 'more bio here'
cool_bar.imageUrl = 'cool_bar.png'
cool_bar.ratings = Rating(sex_ration=3, opposite_sex_popularit=5, match_ratio=10)
cool_bar.users_list = []

all_events = [dizzy, el_vacino, inga, drink_point, cool_bar]

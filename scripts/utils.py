import sys
import argparse

sys.path.append('/var/www/meetme')
from scripts.users import all_users
from scripts.events import all_events
# from server import static_dns, version
from dal.db import DB

debug_mode = False
static_dns = 'ec2-18-217-0-234.us-east-2.compute.amazonaws.com'


class Utils:

    def __init__(self, db):
        self.db = db

    def insert_users(self):
        for user in all_users:
            self.db.insert_user(user)

    # @staticmethod
    # def _fix_event(event):
    #     if debug_mode:
    #         url = 'localhost'
    #     else:
    #         url = static_dns
    #     event['imageUrl'] = 'http://' + url + '/' + version + '/HangoutLocation/img/' + event['imageUrl']
    #     return event

    def insert_events(self):
        for event in all_events:
            # self._fix_event(event)
            self.db.insert_event(event)

    def other_shit(self):
        # self.db.login_user('liorb87', 'Dizzy')
        # self.db.login_user('jacob_zviagin', 'Dizzy')
        # self.db.login_user('bar_refaeli', 'Dizzy')

        # self.db.login_user('liorb87', 'El vacino')
        # self.db.login_user('bar_refaeli', 'El vacino')
        # self.db.login_user('katy_perry', 'El vacino')

        # self.db.login_user('liorb87', 'inga')
        # self.db.login_user('jacob_zviagin', 'inga')
        # self.db.login_user('bar_refaeli', 'inga')
        # self.db.login_user('katy_perry', 'inga')

        self.db.login_user('liorb87', 'Dizzy')
        self.db.login_user('jacob_zviagin', 'Dizzy')
        self.db.login_user('bar_refaeli', 'Dizzy')
        self.db.login_user('katy_perry', 'Dizzy')

        self.db.logout_user('bar_refaeli')

        # self.db.login_user('bar_refaeli', 'Drink Point')
        # self.db.login_user('katy_perry', 'Drink Point')

        match = self.db.like('jacob_zviagin', 'katy_perry')
        match = self.db.like('katy_perry', 'jacob_zviagin')
        match = self.db.like('katy_perry', 'liorb87')

        match = (self.db.check_match('jacob_zviagin', 'katy_perry'))
        if match is not None:
            print('Match: ' + match['userA'], match['userB'], str(match['matchTime']))

        # val = self.db.insert_message('hello', 'jacob_zviagin', 'katy_perry')
        # val = self.db.insert_message('nice to meet you :)', 'katy_perry', 'jacob_zviagin')

        val = self.db.get_all_messages('jacob_zviagin', 'katy_perry')
        print(val)

        val = self.db.create_token('liorb87', '123456')
        val = self.db.get_user_by_token(val)
        pass

    def checkin_user_to_event(self, user_name, event_name):
        user = self.db.get_user_by_name(user_name)
        if user is None:
            print('No such user ' + str(user_name))
            exit(-1)
        event = self.db.get_event_by_name(event_name)
        if event is None:
            print('No such event ' + str(event_name))
            exit(-1)
        try:
            self.db.checkin_user_to_event(user['_id'], event['_id'])
        except Exception as e:
            print(str(e))

    def checkout_user_from_event(self, user_name):
        user = self.db.get_user_by_name(user_name)
        if user is None:
            print('No such user ' + str(user_name))
            exit(-1)
        self.db.checkout_user_from_event(user['_id'])


def main():
    global debug_mode
    parser = argparse.ArgumentParser(description='MeetMe Utils')
    parser.add_argument('-d', '--debug', dest='debug_mode', action='store_true', help='debug mode')
    args = parser.parse_args()

    debug_mode = args.debug_mode
    if debug_mode:
        db = DB()
    else:
        db = DB(static_dns)

    utils = Utils(db)

    utils.insert_events()
    # utils.checkin_user_to_event('facebook_10155297589814352_Lior_Brafman', 'Cool Bar')
    # utils.checkout_user_from_event('facebook_10155297589814352_Lior_Brafman')

    # utils.checkin_user_to_event('facebook_112398272915474_angelina_jolie', 'Cool Bar')
    # utils.checkin_user_to_event('facebook_102881830536381_reese_witherspoon', 'Cool Bar')
    # utils.checkin_user_to_event('facebook_116191282536050_ryan_reynolds', 'Cool Bar')
    # utils.checkin_user_to_event('facebook_109376506552526_chris_evans', 'Cool Bar')


if __name__ == '__main__':
    main()
    # db = DB()
    # db.logout_user('liorb87')

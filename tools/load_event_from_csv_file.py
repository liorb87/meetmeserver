import csv
import sys
from geopy.geocoders import Nominatim

from dal.documents_type.event import Event,Location, Rating


def read_file(file_name):
    events = []
    geolocator = Nominatim()

    with open(file_name, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        head_lines = None
        line_number = 0
        for row in spamreader:
            line_number += 1

            if head_lines is None:
                head_lines = row
                continue
            try:
                event = Event()
                event.name = row[0]
                event.description = row[1]
                event.bio = row[2]
                event.address = row[3]
                event.imageUrl = row[4]

                location = geolocator.geocode(event.address)
                if location is None:
                    print('No lat and lng for ' + event.address, file=sys.stderr)
                    event.location(0, 0)
                else:
                    event.location = Location(location.latitude, location.longitude)
                event.ratings = Rating(10, 10, 10)
                event.users_list = []

                print(event)
            except Exception as e:
                print('Error at line ', line_number, e)


def main():
    read_file('events.csv')

if __name__ == '__main__':
    main()

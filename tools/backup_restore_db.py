import sys
import argparse
import os
# from bson.objectid import ObjectId
from datetime import datetime
from subprocess import call

sys.path.append('/var/www/meetme/')
from dal.db import static_dns, port, user_name, db_password, db_name

mongo_folder = '/home/ubuntu/mongodb-linux-x86_64-ubuntu1604-3.6.2/bin/'


def backup():
    pass
    # ./mongodb-linux-x86_64-ubuntu1604-3.6.2/bin/mongodump --host "ec2-18-217-0-234.us-east-2.compute.amazonaws.com"
    # --port 27017 -u Admin -p manunited99 -d MeetMe --out db_backup/

    folder_name = os.path.join('/home/ubuntu/db_backups', str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S')))
    print('backup folder name: ' + folder_name)
    os.makedirs(folder_name)

    cmd = mongo_folder + 'mongodump --host "' + static_dns + '" --port ' + str(port) + ' -u ' + user_name +\
        ' -p ' + db_password + ' -d ' + db_name + ' --out ' + folder_name
    print(cmd)
    failure = os.system(cmd)
    if failure:
        print('Backup failed!\n')
        return False

    return True


def restore(folder_name):
    # ./mongodb-linux-x86_64-ubuntu1604-3.6.2/bin/mongorestore --host "ec2-18-217-0-234.us-east-2.compute.amazonaws.com"
    # --port 27017 -u Admin -p manunited99 -d MeetMe db_backup/MeetMe/

    cmd = mongo_folder + 'mongorestore --host "' + static_dns + '" --port ' + str(port) + ' -u ' + user_name +\
        ' -p ' + db_password + ' -d ' + db_name + ' /home/ubuntu/db_backups/' + folder_name + '/' + db_name
    print(cmd)
    failure = os.system(cmd)
    if failure:
        print('Restore failed!\n')
        return False
    return True


def main():
    parser = argparse.ArgumentParser(description='MeeloServer')
    parser.add_argument('-m', '--mode', dest='mode', default='backup',
                        help='set mode [backup|restore]')
    parser.add_argument('-bf', '--backup_folder', dest='backup_folder', default='',
                        help='set mode [backup|restore]')
    args = parser.parse_args()

    if args.mode == 'backup':
        backup()
    else:  # restore
        restore(args.backup_folder)


if __name__ == '__main__':
    main()

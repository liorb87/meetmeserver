# pip install falcon
# pip install pyfcm
# pip install facebook-sdk

# export PYTHONPATH=/home/ubuntu/meetmeserver

import socket
import argparse
# import types
import os
import logging
import sys
from flask import Flask, request, json, send_file, redirect
# from bson.json_util import dumps
from bson import ObjectId
from dal.db import DB
from pyfcm import FCMNotification
import facebook
import re
import boto3
import random
import uuid
import urllib
from dal.documents_type.event import Event, Location, Rating
from dal.documents_type.subscribe import Subscribe
from dal.documents_type.user import Sex, User
from dal.db import static_dns

identity_pool_id = "us-east-2:ca6db46a-fdc7-4a73-a1ae-e0d6d70f1d6d"
bucket_name = 'images1meelo'
region_id = "us-east-2"
static_folder = 'web_site'

admin_meelo = {'aws_access_key_id': 'AKIAIU4SGVHV4ODK4PRQ',
               'aws_secret_access_key': '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'}


photos_meelo = {'aws_access_key_id': 'AKIAZOUHH3MQK7CGVP5J',
               'aws_secret_access_key': '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'}


UserPoolId = 'us-east-2_fGNy8X2TO'

s3client = boto3.client(
        's3',
        aws_access_key_id=photos_meelo['aws_access_key_id'],
        aws_secret_access_key=photos_meelo['aws_secret_access_key'],
        region_name=region_id,
    )

client_cognito = boto3.client(
    'cognito-idp',
    aws_access_key_id=admin_meelo['aws_access_key_id'],
    aws_secret_access_key=admin_meelo['aws_secret_access_key'],
    region_name=region_id)

client_sns = boto3.client(
    'sns',
    aws_access_key_id=admin_meelo['aws_access_key_id'],
    aws_secret_access_key=admin_meelo['aws_secret_access_key'],
    region_name='us-east-1',
    )

app = Flask(__name__, static_folder=static_folder)

debug_mode = False
version = 'v0.1'
port = 3000
db = None
pass_code = False

all_methods = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS']


def send_user_notification(user_id, data, notification_type):
    # push_service = FCMNotification(api_key="<api-key>")
    # proxy_dict = {
    #     "http": "http://127.0.0.1",
    #     "https": "http://127.0.0.1",
    # }
    try:
        print('send notification')
        push_service = FCMNotification(api_key="AAAAkY3jSA8:APA91bHLq_PpFX_ABQ58bDnEL2zroKrQHuUddgelZ2RFnhdfOwLEMCb8_zkZxl-TdRGPguRSlAu606QQI4nZ-SrX9v9yfIA8K7CKXfWyufuicdvy3z7AyPugYp4nNsCvu1v_uVh-ToED", proxy_dict=None)
        print('push service created')
        user = db.get_by_id(user_id, DB.users_collection)
        print('got user')
        print(user)
        registration_id = user['deviceToken']
        print('data:')
        print(data)
        # message_title = "Uber update"
        # message_body = "Hi john, your customized news for today is ready"
        # result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
        #                                            message_body=message_body)
        # logging.debug(result)
        dataJson = json.loads(data)
        print('push notification token:')
        print(registration_id)
        dataJson['type'] = notification_type
        realDataMessage = {'message': JSONEncoder().encode(dataJson)}
        registration_ids = [registration_id]
        print('about to push')
        result = push_service.notify_multiple_devices(registration_ids=registration_ids, data_message=realDataMessage)
        print('after push')
        print(result)
        logging.debug(result)
        logging.debug(realDataMessage)
    except Exception as e:
        print('exception')
        print(e)


class JSONEncoder(json.JSONEncoder):

    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


def create_response(response_obj, status):
    response = app.response_class(
        # response=json.dumps(response_obj),
        response=JSONEncoder().encode(response_obj),
        status=status,
        mimetype='application/json'
    )
    print(response)
    return response


# all
def check_authorization():
    print('auth 1');
    if hasattr(request, 'authorization') and \
            hasattr(request.authorization, 'username') and \
            request.authorization.username != '' and \
            hasattr(request.authorization, 'password') and \
            request.authorization.password != '':
        print('auth2')
        user_name = request.authorization.username
        password = request.authorization.password
        user = db.get_user_by_token(db.create_token(user_name, password))
    elif 'HTTP_AUTHORIZATION' in request.environ:
        print('auth3')
        method = request.environ['HTTP_AUTHORIZATION'].split(' ')[0]
        print('auth4')
        token = request.environ['HTTP_AUTHORIZATION'].split(' ')[1]
        logging.debug(method + ': ' + token)

        if method == 'facebookToken':
            print('facebook')
            user = db.get_user_by_facebook_token(token)
            if user is None:
                print('user is none')
                graph = facebook.GraphAPI(access_token=token, version="7.0")
                print('graph success')
                token_info = graph.debug_access_token(token=token,
                                                      app_id='147109385999430',
                                                      app_secret='e8dcb7c28afb6b35a1efd7e8caf0fb7c')
                print('debug access token')
                print('token info') 
                print(token_info)
                if token_info['data']['app_id'] == '147109385999430':
                    profile = graph.get_object('me')
                    facebook_id = profile['id']
                    user = db.get_user_by_facebook_id(facebook_id)
                    if user is not None:
                        print('user is none 2nd time')
                        logging.debug(profile)
                        db.update_facebook_token(user['_id'], token)
        elif method == 'meeloToken':
            user = db.get_user_by_meelo_token(token)
            print(user)
    else:
        user = None

    if user is None:
        print('auth fail')
        return None, create_response({'error_code': -1, 'error_description': 'illegal authorization'}, 400)
    else:
        return user, None


def get_ip():
    if debug_mode:
        return socket.gethostbyname(socket.gethostname())
    else:
        return static_dns


def fix_user(user):
    for i, val in enumerate(user['picList']):
        user['picList'][i] = 'https://s3-' + region_id + '.amazonaws.com/' + bucket_name + '/users/' + \
                             urllib.parse.quote_plus(user['userName']) + '/' + val
    return user


def fix_event(event):
    event['imageUrl'] = 'https://s3-' + region_id + '.amazonaws.com/' + bucket_name + '/event_icons/' + event['imageUrl']
    return event


@app.errorhandler(Exception)
def handle_bad_request(e):
    return create_response({'error_code': -1, 'error_description': 'BadRequest'}, 400)


@app.route('/')
def home():
    return serve_file('index.html')
@app.route('/css/<string:file_name>', methods=['GET'])
def get_css(file_name):
    return serve_file(file_name, 'css')
@app.route('/images/<string:file_name>', methods=['GET'])
def get_images(file_name):
    return serve_file(file_name, 'images')
@app.route('/fonts/<string:file_name>', methods=['GET'])
def get_fonts(file_name):
    return serve_file(file_name, 'fonts')
@app.route('/js/<string:file_name>', methods=['GET'])
def get_js(file_name):
    return serve_file(file_name, 'js')
def serve_file(file_name, folder=''):
    file_name = os.path.join(static_folder, folder, file_name)
    return send_file(file_name, attachment_filename=file_name)


# @app.route('/' + version + '/User/img/<string:user_name>/<string:img__file_name>', methods=['GET'])
# def redirect_user_img(user_name, img__file_name):
    # https://s3-us-east-2.amazonaws.com/meelo/users/facebook_10155297589814352_Lior_Brafman/IMAGE_20180316_210228_1056158651.jpg
    # url = 'https://s3-us-east-2.amazonaws.com/meelo/users/' + user_name + '/' + img__file_name
    # return redirect(url)


# @app.route('/' + version + '/HangoutLocation/img/<string:img__file_name>', methods=['GET'])
# def redirect_hangout_location_img(img__file_name):
    # https://s3-us-east-2.amazonaws.com/meelo/event_icons/dizzy_frishdon.jpg
    # url = 'https://s3-us-east-2.amazonaws.com/meelo/event_icons/' + img__file_name
    # return redirect(url)

@app.route('/' + version + '/Subscribe/', methods=['GET'])
def subscribe_user():
    name = request.args.get('name', '')
    email = request.args.get('email', '')
    subscribe = Subscribe(name, email)
    db.insert_subscribe(subscribe)
    return create_response(subscribe.to_json_obj(), 200)


@app.route('/' + version + '/User/fbToken/', methods=['GET'])
def get_fb_token_for_user():
    firstName = request.args.get('firstName', '')
    lastName = request.args.get('lastName', '')
    user = db.db[DB.users_collection].find_one({'firstName':re.compile(firstName, re.IGNORECASE),
                                                'lastName': re.compile(lastName, re.IGNORECASE)})
    print(user)
    return create_response({'accessToken': user['facebook_token']}, 200)


def check_pass_code():
    global pass_code

    if pass_code:
        passCode = request.args.get('passcode', '')
        if passCode == '':
            return create_response({'error_code': -2, 'error_description': 'passCode is required'}, 400)

        if not db.check_pass_code_exist(passCode):
            return create_response({'error_code': -3, 'error_description': 'passCode is invalid'}, 400)

    return None


def validate_user(user):
    if 'sexualPreference' not in user:
        return False
    if 'sex' not in user:
        return False
    if user['sex'] != Sex.man.name and user['sex'] != Sex.woman.name:
        return False
    if not isinstance(user['sexualPreference'], list):
        return False
    for preference in user['sexualPreference']:
        if preference != Sex.man.name and preference != Sex.woman.name:
            return False

    return True


@app.route('/' + version + '/User/login/', methods=['POST'])
def user_login():
    global pass_code
    # localhost:3000/v0.1/User/login/
    # http://192.168.1.131:3000/v0.1/User/login/?deviceToken=eOJjmWA5QL8:APA91bEghpCxiWRXOlIWkqcMGxXUNpj2PrLwRJ3B3OhIZG37ml9PVG6a_9IpQqLIWHw0cHP49Ns9Vk8ABa2FfRQtoDtANB-jdBd4O-l-S6IQdGde8g4vdo-Qkjw3-9etjZxFraLUsfBM

    ret_response = check_pass_code()
    if ret_response is not None:
        return ret_response

    user, response = check_authorization()

    if user is not None and not validate_user(user):
        db.delete_user(user)
        return create_response({'error_code': -1, 'error_description': 'not registered'}, 400)

    if user is None:
        return create_response({'error_code': -1, 'error_description': 'not registered'}, 400)
    if 'unseen_matches' in user:
        unseen_matches = db.db[DB.match_collection].find({
            "_id": {'$in': user['unseen_matches']}
        })
        new_unseen_matches = []
        for match in unseen_matches:
            new_unseen_matches.append(match['_id'])
        user['unseen_matches'] = new_unseen_matches
        db.db[DB.users_collection].update_one(filter={'_id': user['_id']},
                                              update={'$set': {'unseen_matches': new_unseen_matches, }},
                                              upsert=False)
    deviceToken = request.args.get('deviceToken', '')
    user['deviceToken'] = deviceToken
    db.update_device_token(user['_id'], deviceToken)

    # token = db.create_token(user['userName'], user['password'])

    fix_user(user)
    # user['token'] = token

    return create_response({'User': [user]}, 200)



@app.route('/' + version + '/User/like/', methods=['POST'])
def user_like():
    # localhost:3000/v0.1/User/like/?id=5a6b55180a9e8c25dc8495fa
    print('like')
    user_from, response = check_authorization()
    logging.debug('user_from=' + str(user_from))
    if user_from is None:
        return response
    print('user found')

    user_id = request.args.get('id', '')
    if user_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'id' query parameter was given"}, 400)
    print('dest user id exists')
    user_to = db.get_by_id(ObjectId(user_id), DB.users_collection)
    print('user to found')
    print(user_to)
    logging.debug('user_to=' + str(user_to))
    match = db.like(user_from['_id'], user_to['_id'])
    print('match_created')
    print(match)

    if match is not None:
        print('match is not none')
        db.update_unseen_matches(user_to['_id'], ObjectId(match['_id']))
        print('updated unseen matches')
        user_to = db.get_by_id(ObjectId(user_id), DB.users_collection)
        print('user to found')
        send_user_notification(user_to["_id"], JSONEncoder().encode(
            {'User': [fix_user(user_to), fix_user(user_from)], 'Match': [match]}), 'match')
        print('sent notification')

    return create_response({'Match': [match]}, 200)




@app.route('/' + version + '/User/unlike/', methods=['POST'])
def user_unlike():
    # localhost:3000/v0.1/User/like/?id=5a6b55180a9e8c25dc8495fa

    user_from, response = check_authorization()
    if user_from is None:
        return response

    user_id = request.args.get('id', '')
    if user_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'id' query parameter was given"}, 400)

    user_to = db.get_by_id(ObjectId(user_id), DB.users_collection)
    db.unlike(user_from['_id'], user_to['_id'])
    return create_response({}, 200)


@app.route('/' + version + '/User/block/', methods=['POST'])
def user_block():
    # localhost:3000/v0.1/User/like/?id=5a6b55180a9e8c25dc8495fa

    user_from, response = check_authorization()
    logging.debug('user_from=' + str(user_from))
    if user_from is None:
        return response

    user_id = request.args.get('id', '')
    if user_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'id' query parameter was given"}, 400)

    user_to = db.get_by_id(ObjectId(user_id), DB.users_collection)
    logging.debug('user_to=' + str(user_to))
    block = db.block(user_from['_id'], user_to['_id'])

    return create_response({'status': 'ok'}, 200)


@app.route('/' + version + '/icons/<string:pic_name>', methods=['GET'])
def get_icons(pic_name):
    # localhost:3000/v0.1/icons/inga.jpg
    file_name = os.path.join('icons', pic_name)
    if not os.path.isfile(file_name):
        return create_response({'error_code': -1, 'error_description': 'no such file exist'}, 400)
    return send_file(file_name, attachment_filename=pic_name)


@app.route('/' + version + '/upload/user_photo/', methods=['POST'])
def upload_photo():

    print('uploading')
    user, response = check_authorization()
    if user is None:
        print('auth failed')
        return response
    print('after user check')
    # localhost:3000/v0.1/icons/inga.jpg

    # check if the post request has the file part
    if 'file' not in request.files:
        print('file not attached')
        return create_response({'error_code': -1, 'error_description': 'no file attached'}, 400)
    file = request.files['file']
    # file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
    # if user does not select file, browser also
    # submit a empty part without filename
    print('after 1st file check')
    if file.filename == '':
        print('no file 2')
        return create_response({'error_code': -1, 'error_description': 'no file attached'}, 400)

    global s3client
    print('before upload')
    s3client.upload_fileobj(file, bucket_name, 'users/' + user['userName'] + '/' + file.filename)
    print('after upload')
    fileUrl = 'https://s3-' + region_id + '.amazonaws.com/' + bucket_name + '/users/' + user['userName'] + '/' + file.filename
    return create_response({'file': fileUrl}, 200)


@app.route('/'+version+'/users_pic/<string:user_name>/<string:pic_name>', methods=['GET'])
def get_users_pic(user_name, pic_name):
    # localhost:3000/v0.1/users_pic/liorb87/1.jpg
    logging.debug(os.path.dirname(__file__))
    # file_name = os.path.join('users_pic', user_name, pic_name)
    # file_name = os.path.dirname(__file__) + '/users_pic' + '/' + user_name + '/' + pic_name
    # file_name = os.path.join('bio.txt')
    file_name = os.path.join('users_pic', user_name, pic_name)
    logging.debug(os.listdir("."))
    logging.debug(os.path.abspath(file_name))
    if not os.path.isfile(file_name):
        return create_response({'error_code': -1, 'error_description': 'no such file exist'}, 400)
    return send_file(file_name, attachment_filename=pic_name)


@app.route("/"+version+"/User/by_event_id/", methods=['POST'])
def get_all_users_in_event():
    # localhost:3000/v0.1/User/by_event_id/?event_id=5a6b55180a9e8c25dc8495fd

    user, response = check_authorization()
    if user is None:
        return response

    event_id = request.args.get('event_id', '')
    if event_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'event_id' query parameter was given"}, 400)

    event = db.get_by_id(event_id, DB.events_collection)

    if event is None:
        return create_response({'error_code': -1, 'error_description': 'no such event exist'}, 400)

    blocked_ids = list()

    blocked_ids.append(user['_id'])
    query = {'$and': [
        {'hangoutLocationId': event['_id'],
         'sex': {'$in': user['sexualPreference']},
         # 'sexualPreference': user['sex'],
         '_id':{'$nin': blocked_ids}
         }
    ]}
    users = db.db[DB.users_collection].find(query)
    event_users = []
    for event_user in users:
        event_users.append(fix_user(event_user))

    return create_response({'User': event_users}, 200)


@app.route("/"+version+"/User/by_event_id_recent_visitors/", methods=['POST'])
def get_recent_visitors_in_event():
    # localhost:3000/v0.1/User/by_event_id/?event_id=5a6b55180a9e8c25dc8495fd

    user, response = check_authorization()
    if user is None:
        return response

    event_id = request.args.get('event_id', '')
    if event_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'event_id' query parameter was given"}, 400)

    recent_user_ids = db.db[DB.checkin_collection].find({'eventId': ObjectId(event_id)}, {'userId': 1})
    user_ids_array = []
    for recent_user_id in recent_user_ids:
        if not user['_id'] == recent_user_id['userId']:
            user_ids_array.append(recent_user_id['userId'])

    query = {'$and': [
        {
         'sex': {'$in': user['sexualPreference']},
         'sexualPreference': user['sex'],
         '_id':{'$in': user_ids_array}
         }
    ]}
    users = db.db[DB.users_collection].find(query)
    event_users = []
    for event_user in users:
        event_users.append(fix_user(event_user))
    return create_response({'User': event_users}, 200)


@app.route("/"+version+"/User/suggestions_in_event/", methods=['POST'])
def get_suggestions():
    # localhost:3000/v0.1/User/by_event_id/?event_id=5a6b55180a9e8c25dc8495fd
    print('getting matches')
    user, response = check_authorization()
    if user is None:
        return response
    unlikes = db.db[DB.unlikes_collection].find({'userA': user['_id']})
    blocked_ids = []
    for unliked_user in unlikes:
        blocked_ids.append(unliked_user['userB'])

    matches = db.db[DB.match_collection].find({'$or': [{'userA': user['_id']},
                                                       {'userB': user['_id']}]})
    for match in matches:
        if match['userA'] == user['_id']:
            blocked_ids.append(match['userB'])
        else:
            if match['active'] is True:
                blocked_ids.append(match['userA'])
    blocked_ids.append(user['_id'])
    print('user event id')
    print(user['hangoutLocationId'])
    query = {'$and': [
        {'hangoutLocationId': user['hangoutLocationId'],
         'sex': {'$in': user['sexualPreference']},
         'sexualPreference': user['sex'],
         '_id':{'$nin': blocked_ids}
         }
    ]}
    users = db.db[DB.users_collection].find(query)
    event_users = []
    for event_user in users:
        print(event_user['userName'])
        event_users.append(fix_user(event_user))

    return create_response({'User': event_users}, 200)


@app.route("/"+version+"/User/", methods=['POST'])
def get_user():
    # localhost:3000/v0.1/user/?userName=3
    userName = request.args.get('userName', '')
    if userName == '':
        return create_response({'error_code': -1, 'error_description': "no 'userName' query parameter was given"}, 400)

    user = db.get_user_by_name(userName)
    if user is None:
        return create_response({'error_code': -1, 'error_description': 'no such user exist'}, 400)

    fix_user(user)

    return create_response(user, 200)


@app.route('/' + version + '/HangoutLocation/add/', methods=['POST'])
def event_add():
    try:
        event = request.get_json()
    except:
        return create_response({'error_code': -1, 'error_description': 'json object is not valid'}, 400)
    # if 'file' not in request.files:
    #     return create_response({'error_code': -1, 'error_description': 'no file attached'}, 400)
    # file = request.files['file']
    # file = request.files

    try:
        event = Event(event['name'],
                      address=event['address'],
                      description=event['description'],
                      location=Location(event['location']['lat'], event['location']['lng']),
                      bio=event['bio'],
                      imageUrl=event['imageUrl'],
                      ratings=Rating(event['ratings']['sex_ration'],
                                     event['ratings']['opposite_sex_popularit'],
                                     event['ratings']['match_ratio']),
                      users_list=event['users_list'])
        db.insert_event(event)
    except:
        return create_response({'error_code': -1, 'error_description': 'event object is not valid'}, 400)

    return create_response({'Status': 'Ok'}, 200)


@app.route('/'+version+'/HangoutLocation/update_user_location/', methods=['POST'])
def update_user_location():
    # localhost:3000/v0.1/HangoutLocation/?radius=10&lat=32.186312&lng=34.812172

    user, response = check_authorization()
    if user is None:
        return response

    lat = request.args.get('lat', '')
    lng = request.args.get('lng', '')

    if lat == '' or lng == '':
        return create_response({'error_code': -1, 'error_description': "no'lat' or 'lng' query parameter was given"}, 400)

    found = False
    events = db.get_events_by_loc(2, lat, lng)
    user_event = db.get_by_id(user['hangoutLocationId'], DB.events_collection)
    for event in events:
        if event['_id'] == user['hangoutLocationId']:
            found = True

    if (found is False) and (user['hangoutLocationId'] is not None):
        db.db[DB.users_collection].update_one(filter={'_id': user['_id']},
                                              update={'$set': {'hangoutLocationId': None, }},
                                              upsert=False)
        send_user_notification(user["_id"], JSONEncoder().encode(
            {'HangoutLocation': [fix_event(user_event)]}), 'checkOut')

    return create_response({'status': 'ok'}, 200)


@app.route('/'+version+'/HangoutLocation/', methods=['POST'])
def get_all_events_by_location():
    # localhost:3000/v0.1/HangoutLocation/?radius=10&lat=32.186312&lng=34.812172
    print('get by radius')
    user, response = check_authorization()
    if user is None:
        print('hangout auth failed')
        return response
    print('auth success')
    print(request.args)
    radius = request.args.get('radius', '')
    print('auth success2')
    lat = request.args.get('lat', '')
    print('auth success3')
    lng = request.args.get('lng', '')
    print('auth success4')

    if radius == '' or lat == '' or lng == '':
        print('param error')
        return create_response({'error_code': -1, 'error_description': "no 'radius' or 'lat' or 'lng' query parameter was given"}, 400)

    events = db.get_events_by_loc(radius, lat, lng)
    print(events)
    for event in events:
        fix_event(event)
    print('returning response')
    return create_response({'hangoutLocation': events}, 200)


@app.route('/'+version+'/HangoutLocation/checkIn/', methods=['POST'])
def event_checkin():
    # localhost:3000/v0.1/HangoutLocation/checkIn?id=Dizzy

    user, response = check_authorization()
    if user is None:
        return response

    event_id = request.args.get('id', '')
    if event_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'id' query parameter was given"}, 400)

    try:
        db.checkin_user_to_event(user['_id'], event_id)
        event = db.get_by_id(event_id, db.events_collection)
        fix_event(event)
        response = create_response({'hangoutLocation': [event]}, 200)
    except Exception as e:
        response = create_response({'error_code': -1, 'error_description': str(e)}, 400)
    return response


@app.route('/'+version+'/HangoutLocation/searchByName/', methods=['POST'])
def by_name():
    # localhost:3000/v0.1/HangoutLocation/checkIn?id=Dizzy

    user, response = check_authorization()
    if user is None:
        return response
    name = request.args.get('name', '')
    if name == '':
        return create_response({'error_code': -1, 'error_description': "no 'name' query parameter was given"}, 400)
    name = re.escape(name)
    length = len(name)
    logging.debug(length)
    upperName = name.upper()
    lowerName = name.lower()
    regex = '.*'
    i = 0
    while i < length:
        if upperName[i] == '\\':
            # regex = regex + '[' + upperName[i] + upperName[i +1] + lowerName[i] + lowerName[i + 1] + ']' + '.*'
            i += 2
        else:
            regex = regex + '[' + upperName[i] + lowerName[i] + ']' + '.*'
            i += 1
    logging.debug(regex)
    events = db.db[DB.events_collection].find({'name': {'$regex': regex}})
    fixedEvents = []
    for fixedEvent in events:
        fixedEvents.append(fix_event(fixedEvent))
    return create_response({'hangoutLocation': fixedEvents}, 200)


@app.route('/'+version+'/HangoutLocation/checkedIn/', methods=['POST'])
def event_checkedin():
    # localhost:3000/v0.1/HangoutLocation/checedkIn
    user, response = check_authorization()
    if user is None:
        return response

    if 'hangoutLocationId' in user:
        if not user['hangoutLocationId'] is None:
            event = db.get_by_id(user['hangoutLocationId'], DB.events_collection)
            res = []
            if event is not None:
                res.append(fix_event(event))

                return create_response({'hangoutLocation': res}, 200)

    return create_response({'status': 'not_checked_in'}, 200)


@app.route('/'+version+'/HangoutLocation/checkOut/', methods=['POST'])
def event_checkout():
    # localhost:3000/v0.1/HangoutLocation/checkedOut
    print('checkOut')
    user, response = check_authorization()

    if user is None:
        print('user not found')
        return response
    print('user  found')

    try:
        db.checkout_user_from_event(user['_id'])
        response = create_response({'status': 'OK'}, 200)
    except Exception as e:
        print('error')
        print(e)
        response = create_response({'error_code': -1, 'error_description': str(e)}, 400)
    return response


@app.route('/'+version+'/Match/', methods=['POST'])
def all_matches():
    # localhost:3000/v0.1/HangoutLocation/checkIn?id=Dizzy

    user, response = check_authorization()
    if user is None:
        return response

    matches, messages, users = db.find_active_matches_by_user_id(user['_id'])
    if matches is None:
        return create_response({'Match': []}, 200)
    fixedUsers = []
    for unfixedUser in users:
        fixedUsers.append(fix_user(unfixedUser))

    return create_response({'Match': matches, 'Message': messages, 'User': fixedUsers}, 200)


@app.route('/'+version+'/Message/', methods=['POST'])
def message_last():
    # localhost:3000/v0.1/HangoutLocation/checkedOut

    match_id = request.args.get('match_id', '')
    limit = int(request.args.get('limit', '1'))
    page = int(request.args.get('page', '0'))
    if match_id == '':
        return create_response({'error_code': -1, 'error_description': "no 'match_id' query parameter was given"}, 400)

    user, response = check_authorization()
    if user is None:
        return response
    logging.debug('clear unseen for user' + user['userName'])
    messages = db.find_message_by_match_id(match_id, page, limit)
    db.update_unseen_matches_clear(user['_id'], ObjectId(match_id))
    user = db.get_by_id(user['_id'], DB.users_collection)
    return create_response({'Message': messages, 'User': [fix_user(user)]}, 200)


@app.route('/' + version + '/Message/add/', methods=['POST'])
def message_add():
    # localhost:3000/v0.1/HangoutLocation/checkedOut
    message = request.args.get('entity', '')
    if message == '':
        return create_response({'error_code': -1, 'error_description': "no 'entity' query parameter was given"}, 400)
    match_id = json.loads(message)['matchId']
    text = json.loads(message)['text']
    direction = json.loads(message)['direction']

    user, response = check_authorization()
    if user is None:
        return response

    insertedMessage = db.add_message(match_id, direction, text)
    match = db.get_by_id(ObjectId(match_id), db.match_collection)

    if match["userA"] == user['_id']:
        send_to_user_id = match["userB"]
        sender_user_id = match["userA"]
    else:
        send_to_user_id = match["userA"]
        sender_user_id = match["userB"]

    query = {'$or': [
        {'userA': sender_user_id, 'userB': send_to_user_id},
        {'userA': send_to_user_id, 'userB': sender_user_id}
    ]}
    blocks = db.db[DB.block_collection].find(query)
    if blocks.count() == 0:
        db.update_unseen_matches(send_to_user_id, ObjectId(match_id))
        send_to_user = db.get_by_id(send_to_user_id, db.users_collection)
        sender_user = db.get_by_id(sender_user_id, db.users_collection)
        send_user_notification(send_to_user_id, JSONEncoder().encode({'Message': [insertedMessage],
                                                                      'User': [fix_user(send_to_user),
                                                                               fix_user(sender_user)],
                                                                      'Match': [match]}), 'message')
    return create_response({'Message': []}, 200)


@app.route('/' + version + '/User/update/', methods=['POST'])
def user_update():
    user, response = check_authorization()
    if user is None:
        return response
    # localhost:3000/v0.1/HangoutLocation/checkedOut
    new_user = request.args.get('entity', '')
    if new_user == '':
        return create_response({'error_code': -1, 'error_description': "no 'entity' query parameter was given"}, 400)
    user_id = user['_id']

    new_user = json.loads(new_user)
    sex = new_user['sex']
    sexualPreference = new_user['sexualPreference']
    picList = new_user['picList']
    shortDescription = new_user['shortDescription']
    firstName = new_user['firstName']
    lastName = new_user['lastName']
    birthdate = new_user['birthdate']

    ret_val = db.update_user(user_id, sex, sexualPreference, picList, shortDescription, firstName, lastName, birthdate)

    return create_response({'User': [fix_user(ret_val)]}, 200)


@app.route('/' + version + '/User/add/', methods=['POST'])
def user_add():
    if 'HTTP_AUTHORIZATION' in request.environ:
        facebook_token = request.environ['HTTP_AUTHORIZATION'].split(' ')[1]
    # localhost:3000/v0.1/HangoutLocation/checkedOut

    ret_response = check_pass_code()
    if ret_response is not None:
        return ret_response

    new_user = request.args.get('entity', '')
    if new_user == '':
        return create_response({'error_code': -1, 'error_description': "no 'entity' query parameter was given"}, 400)

    user = json.loads(new_user)
    user['facebook_token'] = user['facebook_id']
    # setattr(user,'to_json_obj',types.MethodType(to_json_obj, user))
    user_id = db.insert_data_json_object(user, db.users_collection)
    added_user = db.get_by_id(user_id, db.users_collection)
    # facebook_id = json.loads(new_user)['facebook_id']
    # user_name = json.loads(new_user)['userName']
    # sex = json.loads(new_user)['sex']
    # sexualPreference = json.loads(new_user)['sexualPreference']
    # picList = json.loads(new_user)['picList']
    # shortDescription = json.loads(new_user)['shortDescription']
    # ret_val = db.add_user(user_name, facebook_id, facebook_token, sex,sexualPreference, picList, shortDescription)

    return create_response({'User': [fix_user(added_user)]}, 200)


@app.route('/poll/', methods=['GET'])
def poll():
    return create_response({'commands': [{"commandId": 1}, {"commandId": 2}, {"commandId": 3}]}, 200)


@app.route('/' + version + '/User/isPasscodeRequired/', methods=['POST'])
def isPasscodeRequired():
    global pass_code
    return create_response({'required': False}, 200)


@app.route('/' + version + '/User/verify_passcode/', methods=['POST'])
def verify_passcode():
    global pass_code

    print(pass_code)

    if pass_code:
        print('pass required')
        pass_code_verify = request.args.get('passCode', '')
        if pass_code_verify == '':
            print('no pass code')
            return create_response({'error_code': -1,
                                    'error_description': "no 'pass_code_verify' query parameter was given"}, 400)

        pass_code_verify = pass_code_verify.strip()
        if not db.check_pass_code_exist(pass_code_verify):
            print('pass code db error')
            return create_response({'error_code': -3, 'error_description': 'passCode is invalid'}, 400)

    return create_response({'status': 'OK'}, 200)


@app.route("/*", methods=all_methods)
def not_route():
    pass


@app.route('/' + version + '/User/login_with_phone_number/', methods=['POST'])
def login_with_phone_number():
    phone_number = request.args.get('phone_number', '')
    if phone_number == '':
        return create_response({'error_code': -1,
                                'error_description': "no 'phone_number' query parameter was given"}, 400)

    phone_number = phone_number.strip()
    if str(phone_number)[0] != '+':
        phone_number = '+'+phone_number

    ret_response = check_pass_code()
    if ret_response is not None:
        return ret_response

    user = db.get_user_by_phone_number(phone_number)
    if user is None:
        user = User()
        user = user.to_json_obj()
        user['userName'] = phone_number
        user['phoneNumber'] = phone_number
        user['sms_code'] = str(random.randint(100000, 999999))
        user['meelo_token'] = uuid.uuid4().hex
        db.insert_data_json_object(user, db.users_collection)
    else:
        sms_code = str(random.randint(100000, 999999))
        user = db.update_user_sms_code(phone_number, sms_code)

    try:
        send_sms(user['userName'], 'Your Meelo code is ' + user['sms_code'])
    except Exception as e:
        return create_response({'error_code': -2,
                                'error_description': str(e)}, 400)

    return create_response({'status': 'SMS was sent to ' + phone_number}, 200)


def send_sms(phone_number, msg):
    global client_sns

    print('sms was sent to ' + phone_number)
    response = client_sns.publish(
        PhoneNumber=phone_number,
        # Message='ברוך הבא לMeelo, הקוד הסודי שלך הוא ' + str(password)
        Message=msg
        # MessageStructure='string' (Optional)
    )
    return response


@app.route('/' + version + '/User/verify_phone_number/', methods=['POST'])
def verify_phone_number():
    phone_number = request.args.get('phone_number', '')
    print('verify phone '+ phone_number)
    if phone_number == '':
        return create_response({'error_code': -1,
                                'error_description': "no 'phone_number' query parameter was given"}, 400)
    phone_number = phone_number.strip()
    if str(phone_number)[0] != '+':
        phone_number = '+' + phone_number

    sms_code = request.args.get('sms_code', '')
    print('sms code ', sms_code)
    if sms_code == '':
        return create_response({'error_code': -2,
                                'error_description': "no 'sms_code' query parameter was given"}, 400)

    user = db.get_user_by_phone_number(phone_number)
    print('user:')
    print(user)
    if user is None:
        return create_response({'error_code': -3,
                                'error_description': "user with phone number " + phone_number +" does not exist"}, 400)

    if user['sms_code'] != sms_code:
        return create_response({'error_code': -4,
                                'error_description': "sms code doesn't match"}, 400)
    print('after compare sms')

    ret_response = check_pass_code()
    if ret_response is not None:
        return ret_response
    print('after check pass code')

    return create_response({'User': [user]}, 200)


def login_with_phone_number_old(phone_number):
    global client_cognito

    password = random.randint(100000, 999999)

    try:
        response = client_cognito.admin_create_user(
            UserPoolId='us-east-2_fGNy8X2TO',
            Username=phone_number,
            UserAttributes=[],
            ValidationData=[],
            # TemporaryPassword='Q!1w2e3r4',
            TemporaryPassword=str(password),
            ForceAliasCreation=True,
            # MessageAction='RESEND',
            DesiredDeliveryMediums=[
                'SMS',
            ]
        )
        return response
    except Exception as e:
        msg = e.response['Error']['Message']
        print(msg)
        return False


def main():
    global debug_mode
    global db
    #global pass_code
    parser = argparse.ArgumentParser(description='MeetMeServer')
    parser.add_argument('-d', '--debug', dest='debug_mode', action='store_true', help='debug mode')
    parser.add_argument('-a', '--aws', dest='aws_db', action='store_true', help='use aws db')
    parser.add_argument('-l', '--logging_level', dest='logging_level', default='DEBUG',
                        help='set logging level [CRITICAL|ERROR|WARN|WARNING|INFO|DEBUG|NOTSET]')
    #parser.add_argument('-p', '--pass_code', dest='pass_code', action='store_false', help='pass code require')
    args = parser.parse_args()

    logger = logging.getLogger(__name__)
    logger.setLevel(args.logging_level)
    logging.basicConfig(filename='meet_me_server.log',
                        level=args.logging_level,
                        format='%(asctime)s | %(levelname)s | PID %(process)d | %(filename)s | line %(lineno)s | %(message)s')
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    #  /etc/uwsgi/vassals/meet_me_server.log
    debug_mode = args.debug_mode
    #pass_code = args.pass_code
    if debug_mode:
        if args.aws_db:
            db = DB(static_dns)
        else:
            db = DB()
        # UPLOAD_FOLDER = 'C:\\Users\\liorb\\Downloads\\meelo_uploads'
        # app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

        # global client_cognito
        # client_cognito.admin_delete_user(UserPoolId='us-east-2_fGNy8X2TO', Username='+972524248350')
        # return

        app.run(host=socket.gethostbyname(socket.gethostname()), port=3000, debug=True)

    else:
        db = DB(static_dns)


main()
# res = send_sms('+972524248350', 'feel good')
# del(res['ResponseMetadata']['HTTPHeaders'])
# print(json.dumps(res, indent=4, sort_keys=True))
# pass

class Checkin:
    def __init__(self, userId, eventId , time):
        self.userId = userId
        self.eventId = eventId
        self.time = time


    def to_json_obj(self):
        json_obj = {'userId': self.userId,
                    'eventId': self.eventId,
                    'time': self.time
                    }
        return json_obj

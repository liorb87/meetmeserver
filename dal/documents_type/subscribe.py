class Subscribe:
    def __init__(self, name, email):
        self.name = name
        self.email = email

    def to_json_obj(self):
        json_obj = {'name': self.name,
                    'email': self.email
                    }
        return json_obj

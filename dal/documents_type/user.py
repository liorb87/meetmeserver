from dal.enums.sex import Sex


class AgePreference:
    def __init__(self, _min, _max):
        self.min = _min
        self.max = _max

    def to_json_obj(self):
        json_obj = {'min': self.min,
                    'max': self.max}
        return json_obj


class Like:
    def __init__(self, userId, eventId, datetime):
        self.userId = userId
        self.eventId = eventId
        self.datetime = datetime

    def to_json_obj(self):
        json_obj = {'userId': self.userId,
                    'event_id': self.eventId,
                    'datetime': str(self.datetime)}
        return json_obj


class User:
    def __init__(self):
        self.userName = None
        self.password = None
        self.facebook_id = None
        self.facebook_token = None
        self.firstName = None
        self.lastName = None
        self.birthdate = None
        self.shortDescription = None
        self.picList = []
        self.sex = None
        self.sexualPreference = None
        self.AgePreference = None

    def to_json_obj(self):
        json_obj = {}
        for attr in self.__dict__:
            value = getattr(self, attr)
            if attr == 'birthdate' and self.birthdate is not None:
                value = str(value)
            elif attr == 'sex' and self.sex is not None:
                value = value.name
            elif attr == 'sexualPreference' and self.sexualPreference is not None:
                arr = []
                for sex in value:
                    arr.append(sex.name)
                value = arr
            elif attr == 'AgePreference' and self.AgePreference is not None:
                value = value.to_json_obj()
            json_obj[attr] = value

        return json_obj


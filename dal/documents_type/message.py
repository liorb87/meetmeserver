class Message:
    def __init__(self,matchId, direction: bool, text, msgTime):
        # macth id
        self.matchId = matchId

        self.direction = direction
        self.text = text
        self.msgTime = msgTime

    def to_json_obj(self):
        json_obj = {'matchId': self.matchId,
                    'direction': self.direction,
                    'text': self.text,
                    'msgTime': str(self.msgTime)}
        return json_obj

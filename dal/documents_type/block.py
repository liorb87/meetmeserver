class Block:
    def __init__(self, userA, userB):
        self.userA = userA
        self.userB = userB

    def to_json_obj(self):
        json_obj = {'userA': self.userA,
                    'userB': self.userB

                    }
        return json_obj

class Location:
    def __init__(self, lat, lng):
        self.lat = lat
        self.lng = lng

    def to_json_obj(self):
        json_obj = {'lat': self.lat,
                    'lng': self.lng}
        return json_obj


class Rating:
    def __init__(self, sex_ration, opposite_sex_popularit, match_ratio):
        self.sex_ration = sex_ration
        self.opposite_sex_popularit = opposite_sex_popularit
        self.match_ratio = match_ratio
        self.general = int((self.sex_ration+self.opposite_sex_popularit+self.match_ratio)/3)

    def to_json_obj(self):
        json_obj = {'sex_ration': self.sex_ration,
                    'opposite_sex_popularit': self.opposite_sex_popularit,
                    'match_ratio': self.match_ratio,
                    'general': self.general}
        return json_obj


class Event:
    def __init__(self, name=None, address=None, description=None, location=None, bio=None, imageUrl=None,
                 ratings=None, users_list=None):
        self.name = name
        self.address = address
        self.description = description
        self.location = location
        self.bio = bio
        self.imageUrl = imageUrl
        self.ratings = ratings
        self.users_list = users_list

    def to_json_obj(self):
        json_obj = {}
        for attr in self.__dict__:
            value = getattr(self, attr)

            if attr == 'location':
                value = value.to_json_obj()
            elif attr == 'ratings':
                value = value.to_json_obj()

            json_obj[attr] = value

        return json_obj

    def __str__(self):
        ret_st = ''
        ret_st += 'name=' + self.name
        ret_st += ', address=' + self.address
        ret_st += ', description=' + self.description
        ret_st += ', bio=' + self.bio
        ret_st += ', imageUrl=' + self.imageUrl
        return ret_st

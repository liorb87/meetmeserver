class Unlike:
    def __init__(self, userA, userB, matchTime, locationId):
        self.userA = userA
        self.userB = userB
        self.matchTime = matchTime
        self.locationId = locationId


    def to_json_obj(self):
        json_obj = {'userA': self.userA,
                    'userB': self.userB,
                    'matchTime': self.matchTime,
                    'locationId': self.locationId
                    }
        return json_obj

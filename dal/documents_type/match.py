class Match:
    def __init__(self, userA, userB, matchTime, locationId, active):
        self.userA = userA
        self.userB = userB
        self.matchTime = matchTime
        self.locationId = locationId
        self.active = active

    def to_json_obj(self):
        json_obj = {'userA': self.userA,
                    'userB': self.userB,
                    'matchTime': self.matchTime,
                    'locationId': self.locationId,
                    'active': self.active
                    }
        return json_obj

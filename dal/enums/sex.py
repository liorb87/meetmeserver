from enum import Enum


class Sex(Enum):
    man = 0
    woman = 1


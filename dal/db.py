# pip install pymongo
# pip install geopy
# C:\Program Files\MongoDB\Server\3.6\bin\mongod.exe --auth --dbpath C:\mongo_db\data\db
# C:\Program Files\MongoDB\Server\3.6\bin\mongod.exe --dbpath C:\mongo_db\data\db

from datetime import datetime
import base64
import logging
from pymongo import MongoClient
import pymongo
from bson.objectid import ObjectId
from geopy import distance


# from dal.documents_type.user import User, AgePreference, Like
from dal.documents_type.match import Match
from dal.documents_type.unlike import Unlike
from dal.documents_type.message import Message
from dal.documents_type.checkin import Checkin
from dal.documents_type.block import Block
from dal.documents_type.subscribe import Subscribe
# from server import JSONEncoder
# from dal.enums.sex import Sex

url = 'mongodb://localhost:27017/MeetMe'

port = 27017
user_name = 'admin'
db_password = 'manunited99'
db_name = 'MeetMe'
static_dns = 'ec2-18-217-89-47.us-east-2.compute.amazonaws.com'


class DB:
    host = 'cluster0-7vcid.mongodb.net'

    folder_name = 'db'
    users_collection = 'users'
    events_collection = 'events'
    match_collection = 'match'
    message_collection = 'message'
    unlikes_collection = 'unlike'
    checkin_collection = 'checkin'
    block_collection = 'block'
    subscribe_collection = 'subscribe'
    pass_code_collection = 'passcode'

    def __init__(self, host='cluster0-7vcid.mongodb.net'):
        print('connecting to db')
        if host == 'localhost':
            print('connecting to local db')
            self.client = MongoClient(host, port, connect=False)
        else:
            con_str = 'mongodb+srv://admin:manunited99@cluster0-7vcid.mongodb.net/test?retryWrites=true&w=majority'
            print('connecting to remote db')
            #connectTimeoutMS=30000, socketTimeoutMS=None, socketKeepAlive=True, connect=False, maxPoolsize=1
            self.client = MongoClient(con_str, connectTimeoutMS=30000, socketTimeoutMS=None, socketKeepAlive=True, connect=False, maxPoolsize=1)
            print(self.client)
            print('done')
            #self.client = MongoClient(host, port, connect=False, username=user_name, password=db_password,
             #authSource=db_name,   authMechanism='SCRAM-SHA-1')

        self.db = self.client[db_name]
        # self.client[db_name].add_user('Lior1987', 'q1w2e3r4', roles=[{'role': 'readWrite', 'db': 'MeetMe'}])
        # self.db.authenticate(user_name, db_password)

    def list_collection_names(self):
        return self.db.list_collection_names()

    def delete_user(self, user):
        ret_val = self.db[DB.users_collection].delete_one({"userName": user['userName']})
        logging.debug('user ' + user.userName + ' deleted ' + ret_val.acknowledged)
        return ret_val.acknowledged

    def insert_user(self, user):
        doc = self.db[DB.users_collection].find_one({"userName": user.userName})
        if doc is None:
            return self.insert_data(user, DB.users_collection)
        else:
            logging.debug('user ' + user.userName + ' is already in the DataBase')

    def insert_event(self, event):
        doc = self.db[DB.events_collection].find_one({'name': event.name})
        if doc is None:
            return self.insert_data(event, DB.events_collection)
        else:
            logging.debug('event ' + event.name + ' is already in the DataBase')

    def insert_subscribe(self, subscribe):
        doc = self.db[DB.subscribe_collection].find_one({'email': subscribe.email})
        if doc is None:
            return self.insert_data(subscribe, DB.subscribe_collection)
        else:
            logging.debug('subscribe [' + subscribe.email + ',' + subscribe.name + '] is already in the DataBase')

    def insert_match(self, match):
        doc = self.db[DB.match_collection].find_one({'userA': match.userA, 'userB': match.userB})
        if doc is None:
            return self.insert_data(match, DB.match_collection)
        else:
            logging.debug('match [' + match.userA + ',' + match.userB + '] is already in the DataBase')

    def insert_data(self, document, collection_name):
        obj = document.to_json_obj()
        ret_val = self.db[collection_name].insert_one(obj)
        logging.debug('1 document inserted to ' + collection_name + ': ' + str(ret_val.acknowledged))
        return ret_val.inserted_id

    def checkin_user_to_event(self, userId, eventId):
        user = self.get_by_id(userId, DB.users_collection)
        if user is None:
            msg = 'userId ' + str(userId) + ' does not exist'
            logging.error(msg)
            raise ValueError(msg)

        if 'hangoutLocationId' in user and user['hangoutLocationId'] is not None:
            msg = 'user ' + str(userId) + 'is already checked in to ' + str(eventId)
            logging.error(msg)
            raise ValueError(msg)

        event = self.get_by_id(eventId, DB.events_collection)
        if event is None:
            msg = 'eventId ' + str(eventId) + ' does not exist'
            logging.error(msg)
            raise ValueError(msg)

        self.db[DB.users_collection].update_one(filter={'_id': user['_id']},
                                                update={'$set': {'hangoutLocationId': ObjectId(eventId)}},
                                                upsert=False)

        checkin = Checkin(userId, eventId, datetime.now())
        self.insert_data(checkin, DB.checkin_collection)

    def checkout_user_from_event(self, userId):
        print('db checkout')
        print(userId)
        user = self.get_by_id(userId, DB.users_collection)
        if user is None:
            print('user not exists')
            msg = 'userId ' + str(userId) + ' does not exist'
            logging.error(msg)
            raise ValueError(msg)
        print('before upsert')
        self.db[DB.users_collection].update_one(filter={'_id': user['_id']},
                                                update={'$set': {'hangoutLocationId': None}},
                                                upsert=False)
        print('after upsert')


    def insert_data_json_object(self, document, collection_name):
        obj = document
        ret_val = self.db[collection_name].insert_one(obj)
        logging.debug('1 document inserted to ' + collection_name + ': ' + str(ret_val.acknowledged))
        return ret_val.inserted_id

    def get_by_id(self, entry_id, collection):
        return self.db[collection].find_one({'_id': ObjectId(entry_id)})

    def get_all_messages(self, userName1, userName2):
        # TODO
        # sort
        # convert to list not like an arab
        query = {'$or': [
            {'userA': userName1, 'userB': userName2},
            {'userA': userName2, 'userB': userName1}
        ]}
        messages = self.db[DB.message_collection].find(query)
        ret_val = []
        for mesggage in messages:
            ret_val.append(mesggage)
        return ret_val

    def get_events_by_loc(self, radius, lat, lng):
        print('db find loc')
        loc = (lat, lng)
        print('2')
        events = self.db[DB.events_collection].find()
        print(events)
        event_list = []
        for event in events:
            print(event)
            event_loc = (event['location']['lat'], event['location']['lng'])
            print(event_loc)
            dis = distance.geodesic(loc, event_loc).km
            print(dis)
            if dis <= float(radius):
                event['distance'] = dis
                event_list.append(event)

        return event_list

    def checkedIn_to(self, user):
        return self.db[DB.events_collection].find_one({"_id": ObjectId(user['hangoutLocationId'])})

    def login_user(self, userId, eventId):
        user = self.get_by_id(userId, DB.users_collection)
        event = self.get_by_id(eventId, DB.events_collection)

        if self.db[DB.events_collection].find_one({"users_list": userId}) is not None:
            logging.debug('user ' + self.get_by_id(userId, DB.users_collection)['userName'] + ' is logged in')
            return False

        if user is None or event is None:
            logging.error('no such user or event')
            return False

        users_list = event['users_list']
        if userId in users_list:
            logging.debug('user ' + userId + ' is already logged in')
            return True
        users_list = set(event['users_list'])
        users_list.add(user['_id'])
        users_list = list(users_list)
        event['users_list'] = users_list

        ret_val = self.db[DB.events_collection].update_one(filter={'_id': eventId},
                                                           update={'$set': {'users_list': event['users_list']}},
                                                           upsert=False)
        logging.debug('user ' + self.get_by_id(userId, DB.users_collection)['userName'] + ' was logged in to ' + self.get_by_id(eventId,DB.events_collection)['name'] + ': ' + str(ret_val.acknowledged))

        return ret_val.acknowledged

    def get_user_by_phone_number(self, phoneNumber):
        return self.db[DB.users_collection].find_one({'phoneNumber': phoneNumber})

    def get_user_by_name(self, userName):
        return self.db[DB.users_collection].find_one({'userName': userName})

    def get_event_by_name(self, eventName):
        return self.db[DB.events_collection].find_one({'name': eventName})

    def create_token(self, userName, password):
        user = self.db[DB.users_collection].find_one({'userName': userName, 'password': password})
        if user is None:
            return None
        else:
            return (base64.b64encode((userName + ':' + password).encode('ascii'))).decode()

    def get_user_by_meelo_token(self, meeloToken):
        return self.db[DB.users_collection].find_one({'meelo_token': meeloToken})

    def get_user_by_facebook_token(self, facebookToken):
        return self.db[DB.users_collection].find_one({'facebook_token': facebookToken})

    def get_user_by_facebook_id(self, facebookId):
        return self.db[DB.users_collection].find_one({'facebook_id': facebookId})

    def get_user_by_token(self, token):
        try:
            userName, password = base64.b64decode(token.encode('ascii')).decode().split(':')
            return self.db[DB.users_collection].find_one({'userName': userName, 'password': password})
        except:
            return None

    def logout_user(self, userName):
        event = self.db[DB.events_collection].find_one({"users_list": userName})
        if event is None:
            logging.error('user ' + userName + ' is not logged in')
            return True

        users_list = event['users_list']
        users_list = set(event['users_list'])
        users_list.remove(userName)
        users_list = list(users_list)
        event['users_list'] = users_list

        ret_val = self.db[DB.events_collection].update_one(filter={'name': event['name']},
                                                           update={'$set': {'users_list': event['users_list']}},
                                                           upsert=False)
        logging.debug('user ' + userName + ' was logged out from ' + event['name'] + ': ' + str(ret_val.acknowledged))

        return ret_val.acknowledged

    def find_match(self, userId1, userId2):
        logging.debug('check_match')
        match = self.db[DB.match_collection].find_one({'userA': userId1, 'userB': userId2})
        if match is not None:
            logging.debug('1: ' + str(match))
            return match
        else:
            match = self.db[DB.match_collection].find_one({'userA': userId2, 'userB': userId1})
            logging.debug('2: ' + str(match))
            return match

    def find_unlike(self, userId1, userId2):
        return self.db[DB.unlikes_collection].find_one({'userA': userId1, 'userB': userId2})

    def find_active_matches_by_user_id(self, userId):

        blocked_ids = []
        blocks = self.db[DB.block_collection].find({'$or': [{'userA': userId},
                                                          {'userB': userId}]})
        for block in blocks:
            if block['userA'] == userId:
                blocked_ids.append(block['userB'])
            else:
                blocked_ids.append(block['userA'])

        query = {'$and': [{'$or': [
            {'$and': [{'userA': userId},
                      {'userB': {'$nin': blocked_ids}}
                      ]},
            {'$and': [{'userB': userId},
                      {'userA': {'$nin': blocked_ids}}
                      ]},

        ]},  {'active': True} ]}

        matches = self.db[DB.match_collection].find(query)
        ret_val = []
        messages = []
        users = []
        for match in matches:
            ret_val.append(match)
            if 'lastMessageId' in match:
                message = self.get_by_id(match['lastMessageId'], DB.message_collection)
                messages.append(message)
            if match['userA'] == userId:
                users.append(self.get_by_id(match['userB'], DB.users_collection))
            else:
                users.append(self.get_by_id(match['userA'], DB.users_collection))

        return ret_val, messages, users

    def find_message_by_match_id(self, matchId, page, limit):
        ret_val = []
        messages = self.db[DB.message_collection].find({'matchId': ObjectId(matchId)}).sort([('msgTime', pymongo.DESCENDING)])[page*limit: page*limit + limit]
        for message in messages:
            ret_val.append(message)
        return ret_val

    def like(self, userId_from, userId_to):
        user_from = self.get_by_id(userId_from, DB.users_collection)
        user_to = self.get_by_id(userId_to, DB.users_collection)
        logging.debug('user_to: ' + str(user_to))
        logging.debug('user_from: ' + str(user_from))
        event = self.checkedIn_to(user_from)
        eventId = event['_id']
        match = self.find_match(userId_from, userId_to)
        if match is not None:
            if match['active'] is True:
                logging.debug('user ' + self.get_by_id(userId_from, DB.users_collection)['userName'] + ' already matched with ' + self.get_by_id(userId_to, DB.users_collection)['userName'] + ' in ' + self.get_by_id(eventId, DB.events_collection)['name'])
                return None
            if match['userA'] == userId_from:
                logging.debug('user ' + self.get_by_id(userId_from, DB.users_collection)['userName'] + ' already liked ' + self.get_by_id(userId_to, DB.users_collection)['userName'] + ' in ' + self.get_by_id(eventId, DB.events_collection)['name'])
                return None
            self.db[DB.match_collection].update_one(filter={'_id': match['_id']},
                                                    update={'$set': {'active': True,
                                                                     'matchTime': datetime.now()}},
                                                    upsert=False)
            return self.get_by_id(match['_id'], DB.match_collection)
        match = Match(user_from['_id'], user_to['_id'], datetime.now(), event['_id'], False)
        self.insert_data(match, DB.match_collection)
        return None

    def block(self, userId_from, userId_to):
        user_from = self.get_by_id(userId_from, DB.users_collection)
        user_to = self.get_by_id(userId_to, DB.users_collection)
        logging.debug('user_to: ' + str(user_to))
        logging.debug('user_from: ' + str(user_from))
        block_obj = Block(user_from['_id'], user_to['_id'])
        self.insert_data(block_obj, DB.block_collection)
        return block_obj

    def unlike(self, userId_from, userId_to):
        user_from = self.get_by_id(userId_from, DB.users_collection)
        user_to = self.get_by_id(userId_to, DB.users_collection)
        logging.debug('user_to: ' + str(user_to))
        logging.debug('user_from: ' + str(user_from))
        event = self.checkedIn_to(user_from)
        eventId = event['_id']
        unlike = self.find_unlike(userId_from, userId_to)
        if unlike is not None:
            logging.debug(
                'user ' + self.get_by_id(userId_from, DB.users_collection)['userName'] + ' already matched with ' +
                self.get_by_id(userId_to, DB.users_collection)['userName'] + ' in ' +
                self.get_by_id(eventId, DB.events_collection)['name'])
            return
        unlike = Unlike(user_from['_id'], user_to['_id'], datetime.now(), event['_id'])
        self.insert_data(unlike, DB.unlikes_collection)
        return

    def add_message(self, matchId, direction, text):
        realMatchId = ObjectId(matchId)
        time = datetime.now()
        message = Message(realMatchId, direction, text, time)
        match = self.get_by_id(realMatchId, DB.match_collection)
        messageId = self.insert_data(message, DB.message_collection)
        # message = self.db[DB.message_collection].find_one({'msgTime':time})
        self.db[DB.match_collection].update_one(filter={'_id': realMatchId},
                                                update={'$set': {'lastMessageId': messageId}},
                                                upsert=False)
        return self.get_by_id(messageId, self.message_collection)

    def update_user(self, user_id,  sex, sexualPreference, picList, shortDescription, firstName, lastName, birthdate):
        self.db[DB.users_collection].update_one(filter={'_id': user_id},
                                                update={'$set': {'sex': sex,
                                                                 'sexualPreference': sexualPreference,
                                                                 'picList': picList,
                                                                 'shortDescription': shortDescription,
                                                                 'firstName': firstName,
                                                                 'lastName': lastName,
                                                                 'birthdate': birthdate}},
                                                upsert=False)
        return self.get_by_id(user_id, DB.users_collection)

    def update_user_sms_code(self, phone_number,  sms_code):
        self.db[DB.users_collection].update_one(filter={'phoneNumber': phone_number},
                                                update={'$set': {'sms_code': sms_code}},
                                                upsert=False)
        return self.get_user_by_phone_number(phone_number)

    def update_device_token(self, user_id, deviceToken):
        self.db[DB.users_collection].update_one(filter={'_id': user_id},
                                                update={'$set': {'deviceToken': deviceToken}},
                                                upsert=False)
        return self.get_by_id(user_id, DB.users_collection)

    def update_facebook_token(self, user_id, facebookToken):
        self.db[DB.users_collection].update_one(filter={'_id': user_id},
                                                update={'$set': {'facebook_token': facebookToken}},
                                                upsert=False)
        return self.get_by_id(user_id, DB.users_collection)

    def update_unseen_matches(self, user_id, match_id):
        user = self.get_by_id(user_id, self.users_collection)
        if 'unseen_matches' in user:
            unseen_matches = user['unseen_matches']
        else:
            unseen_matches = []
        if match_id not in unseen_matches:
            unseen_matches.append(match_id)
        self.db[DB.users_collection].update_one(filter={'_id': user_id},
                                                update={'$set': {'unseen_matches': unseen_matches}},
                                                upsert=False)
        return

    def update_unseen_matches_clear(self, user_id, match_id):
        user = self.get_by_id(user_id, self.users_collection)
        if not'unseen_matches' in user:
            return
        else:
            unseen_matches = user['unseen_matches']
        if match_id not in unseen_matches:
            return
        unseen_matches.remove(match_id)
        self.db[DB.users_collection].update_one(filter={'_id': user_id},
                                                update={'$set': {'unseen_matches': unseen_matches}},
                                                upsert=False)
        return

    def check_pass_code_exist(self, pass_code):
        print('db check pass code')
        print(pass_code)
        print(self.db)
        print(self.client)
        try:
            doc = self.db[DB.pass_code_collection].find_one({'passCode': pass_code})
            print('db check pass code after') 
            if doc is None:
                print('db check pass code not foun')
                return False
            else:
                return True

        except Exception as e:
            print('except')
            print(e)

